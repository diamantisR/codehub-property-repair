jQuery(function ($) {
    $('#login').validate({
        rules: {
            password: {required: true, minlength: 5},
            username: {required: true, email: true}
        },
        messages: {
            password: {
                required: 'Please enter a password',
                minlength: 'Password should be more than 5 digits'
            },
            username: {
                required: 'Please enter an email',
                email: 'Wrong email format'
            }
        }
    });

    $('#create-repair').validate({
        rules: {
            date: {required: true},
            cost: {required: true},
            address: {required: true, minlength: 3}
        },
        messages: {
            date: {
                required: 'Please enter a Date'
            },
            cost: {
                required: 'Please enter Repair Cost'
            },
            address: {
                required: 'Please enter Address',
                minlength: 'Address has to be at least 3 characters'
            }
        },
    });

    $('#edit-repair').validate({
        rules: {
            vat: {required: true, minlength: 9, maxlength: 9},
            date: {required: true},
            cost: {required: true},
            address: {required: true, minlength: 3}
        },
        messages: {
            vat: {
                required: "VAT can't be empty",
                minlength: 'VAT requires 9 digits',
                maxlength: 'VAT requires 9 digits'
            },
            date: {
                required: 'Please enter a Date'
            },
            cost: {
                required: 'Please enter Repair Cost'
            },
            address: {
                required: 'Please enter Address',
                minlength: 'Address has to be at least 3 characters'
            }
        }
    });

    $('#search-repair').validate({
        rules: {
            vat: {minlength: 9, maxlength: 9},
        },
        messages: {
            vat: {
                minlength: 'Vat should be 9 digits',
                maxlength: 'Vat should be 9 digits'
            }
        }
    });

    $('#create-owner').validate({
        rules: {
            vat: {required: true, minlength: 9, maxlength: 9},
            email: {required: true, email: true},
            firstName: {required: true, minlength: 3},
            lastName: {required: true, minlength: 3},
            password: {required: true, minlength: 5},
            address: {required: true, minlength: 3}
        },
        messages: {
            vat: {
                required: "VAT can't be empty",
                minlength: 'VAT requires 9 digits',
                maxlength: 'VAT requires 9 digits'
            },
            email: {
                required: 'Please enter an email',
                email: 'Wrong email format'
            },
            firstName: {
                required: 'Please enter first name',
                minlength: 'At least 3 letters required'
            },
            lastName: {
                required: 'Please enter last name',
                minlength: 'At least 3 letters required'
            },
            password: {
                required: 'Please enter password',
                minlength: 'Password must be more than 5 characters'
            },
            address: {
                required: 'Please enter address',
                minlength: 'Address has to be at least 3 characters'
            }
        }
    });

    $('#edit-owner').validate({
        rules: {
            vat: {required: true, minlength: 9, maxlength: 9},
            email: {required: true, email: true},
            firstName: {required: true, minlength: 3},
            lastName: {required: true, minlength: 3},
            password: {minlength: 5},
            address: {required: true, minlength: 3}
        },
        messages: {
            vat: {
                required: "VAT can't be empty",
                minlength: 'VAT requires 9 digits',
                maxlength: 'VAT requires 9 digits'
            },
            email: {
                required: 'Please enter an email',
                email: 'Wrong email format'
            },
            firstName: {
                required: 'Please enter first name',
                minlength: 'At least 3 letters required'
            },
            lastName: {
                required: 'Please enter last name',
                minlength: 'At least 3 letters required'
            },
            password: {
                minlength: 'Password must be more than 5 characters'
            },
            address: {
                required: 'Please enter address',
                minlength: 'Address has to be at least 3 characters'
            }
        }
    });

    $('#search-owner').validate({
        rules: {
            vat: {minlength: 9, maxlength: 9},
            email: {email: true}
        },
        messages: {
            vat: {
                minlength: 'Vat should be 9 digits',
                maxlength: 'Vat should be 9 digits'
            },
            email: {
                email: 'Wrong email format'
            }
        }
    });

    $(document).ready(function () {
        $("input").click(function () {
            $("input.error").focus(function () {
                var elem = $("label.springError");
                var prevElem = elem.prev();
                if (prevElem.is('input')) {
                    prevElem.removeClass('springError');
                    prevElem.removeClass('error');
                    elem.remove();
                } else {
                    prevElem.prev().removeClass('springError');
                    prevElem.prev().removeClass('error');
                    elem.remove();
                }
            });
        });
    });
});

