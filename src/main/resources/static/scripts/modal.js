jQuery(function ($) {

    $('#modal').on('show.bs.modal', function (event) {
        const id = event.relatedTarget.dataset.id;
        $('#deleteRepairForm').attr('action', `/admin/repairs/${id}/delete`);
        $('.modal-title').text(function () {
            return `Repair #${id}`;
        });
    });

    $('#modalOwner').on('show.bs.modal', function (event) {
        const id = event.relatedTarget.dataset.id;
        $('#deleteOwnerForm').attr('action', `/admin/owners/${id}/delete`);
        $('.modal-title').text(function () {
            return `Owner #${id}`;
        });
    });

});