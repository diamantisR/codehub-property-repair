<#import "/spring.ftl" as spring />

<h4 class="mb-3">Edit Repair</h4>
<form id="edit-repair" action="/admin/repair/${repair.id}/edit" method="post">
    <div class="mb-3">
        <label for="datepicker" class="sr-only">Date</label>
        <@spring.bind "editRepairForm.date"/>
        <input type="date" class="form-control <#if spring.status.errorMessages?has_content>error springError</#if>"
               data-date-format="yyyy-mm-dd" id="datepicker" name="date"
               value="${repair.repairDate}">
        <#list spring.status.errorMessages as error>
            <label class="error springError">${error}</label>
        </#list>
    </div>
    <div class="mb-3">
        <label for="repairStatus">Status</label>
        <@spring.bind "editRepairForm.repairStatus"/>
        <select class="custom-select d-block w-100" id="repairStatus" name="repairStatus"
                value="${repair.repairStatus}">
            <option value="COMPLETE"<#if repair.repairStatus == "COMPLETE"> selected</#if>>Complete</option>
            <option value="ONGOING"<#if repair.repairStatus == "ONGOING"> selected</#if>>In progress</option>
            <option value="PENDING"<#if repair.repairStatus == "PENDING"> selected</#if>>Pending</option>
        </select>
        <#list spring.status.errorMessages as error>
            <label class="error springError">${error}</label>
        </#list>
    </div>
    <div class="mb-3">
        <label for="repairType">Type of repair</label>
        <@spring.bind "editRepairForm.repairType"/>
        <select class="custom-select d-block w-100" id="repairType" name="repairType" value="${repair.repairType}">
            <option value="PAINTING"<#if repair.repairType == "PAINTING"> selected</#if>>Painting</option>
            <option value="INSULATION"<#if repair.repairType == "INSULATION"> selected</#if>>Insulation</option>
            <option value="FRAMES"<#if repair.repairType == "FRAMES"> selected</#if>>Frames</option>
            <option value="ELECTRICAL_SERVICES"<#if repair.repairType == "ELECTRICAL_SERVICES"> selected</#if>>
                Electrical Services
            </option>
            <option value="PLUMBING"<#if repair.repairType == "PLUMBING"> selected</#if>>Plumbing</option>
        </select>
        <#list spring.status.errorMessages as error>
            <label class="error springError">${error}</label>
        </#list>
    </div>

    <div class="mb-3 form-group">
        <label for="address">Address</label>
        <@spring.bind "editRepairForm.address"/>
        <input type="text" id="address" name="address" value="${repair.address}"
               class="form-control <#if spring.status.errorMessages?has_content>error springError</#if>">
        <#list spring.status.errorMessages as error>
            <label class="error springError">${error}</label>
        </#list>
    </div>

    <div class="mb-3">
        <label for="cost" class>Cost</label>
        <@spring.bind "editRepairForm.cost"/>
        <input type="number" min="1" step="1" class="form-control <#if spring.status.errorMessages?has_content>error springError</#if>"
               id="cost" name="cost" value="${repair.cost?c}">
        <#list spring.status.errorMessages as error>
            <label class="error springError">${error}</label>
        </#list>
    </div>

    <div class="mb-3">
        <label for="vat">Owner's VAT</label>
        <@spring.bind "editRepairForm.vat"/>
        <input type="text" class="form-control <#if spring.status.errorMessages?has_content>error springError</#if>"
               id="vat" name="vat" value="${repair.owner.vat}">
        <#list spring.status.errorMessages as error>
            <label class="error springError">${error}</label>
        </#list>
    </div>

    <div class="mb-3">
        <label for="description">Description</label>
        <@spring.bind "editRepairForm.description"/>
        <textarea class="form-control <#if spring.status.errorMessages?has_content>error springError</#if>"
                  id="description" name="description" rows="4"
                  cols="50">${repair.description}</textarea>
        <#list spring.status.errorMessages as error>
            <label class="error springError">${error}</label>
        </#list>
    </div>

    <button class="btn btn-success btn-lg mt-4" type="submit">Update</button>
</form>