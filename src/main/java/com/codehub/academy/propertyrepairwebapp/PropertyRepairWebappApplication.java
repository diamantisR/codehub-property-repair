package com.codehub.academy.propertyrepairwebapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PropertyRepairWebappApplication {

    public static void main(String[] args) {
        SpringApplication.run(PropertyRepairWebappApplication.class, args);
    }

}
