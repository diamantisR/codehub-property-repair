package com.codehub.academy.propertyrepairwebapp.mappers;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.domain.Repair;
import com.codehub.academy.propertyrepairwebapp.enums.RepairStatus;
import com.codehub.academy.propertyrepairwebapp.enums.RepairType;
import com.codehub.academy.propertyrepairwebapp.forms.RepairForm;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class RepairFormToRepairMapper {

    public Repair toRepair(RepairForm repairForm) {

        Repair repair = new Repair();
        repair.setDescription(repairForm.getDescription());
        repair.setCost(repairForm.getCost());
        repair.setAddress(repairForm.getAddress());
        repair.setOwner(new Owner(repairForm.getVat(), repairForm.getLastName(), repairForm.getFirstName()));
        repair.setRepairStatus(RepairStatus.valueOf(repairForm.getRepairStatus()));
        repair.setRepairType(RepairType.valueOf(repairForm.getRepairType()));
        LocalDate date = LocalDate.parse(repairForm.getDate(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        repair.setRepairDate(date);

        return repair;
    }
}
