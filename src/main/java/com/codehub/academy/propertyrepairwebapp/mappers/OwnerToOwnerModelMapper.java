package com.codehub.academy.propertyrepairwebapp.mappers;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.model.OwnerModel;
import org.springframework.stereotype.Component;

@Component
public class OwnerToOwnerModelMapper {

    public OwnerModel mapToOwnerModel(Owner owner) {

        OwnerModel ownerModel = new OwnerModel();
        ownerModel.setEmail(owner.getEmail());
        ownerModel.setPassword("");
        ownerModel.setOwnerType(owner.getOwnerType().getType());
        ownerModel.setVat(owner.getVat());
        ownerModel.setFirstName(owner.getFirstName());

        return ownerModel;
    }
}
