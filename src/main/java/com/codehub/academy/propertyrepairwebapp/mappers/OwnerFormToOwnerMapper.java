package com.codehub.academy.propertyrepairwebapp.mappers;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.enums.OwnerType;
import com.codehub.academy.propertyrepairwebapp.enums.PropertyType;
import com.codehub.academy.propertyrepairwebapp.forms.RegisterUserForm;
import com.codehub.academy.propertyrepairwebapp.forms.UpdateUserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class OwnerFormToOwnerMapper {
    @Autowired
    private PasswordEncoder passwordEncoder;

    public Owner toOwner(RegisterUserForm registerUserForm) {

        Owner owner = new Owner();
        owner.setVat(registerUserForm.getVat());
        owner.setAddress(registerUserForm.getAddress());
        owner.setEmail(registerUserForm.getEmail());
        owner.setOwnerType(OwnerType.valueOf(registerUserForm.getOwnerType()));
        owner.setPropertyType(PropertyType.valueOf(registerUserForm.getPropertyType()));
        owner.setPhoneNumber(registerUserForm.getPhoneNumber());
        owner.setFirstName(registerUserForm.getFirstName());
        owner.setLastName(registerUserForm.getLastName());
        owner.setPassword(passwordEncoder.encode(registerUserForm.getPassword()));

        return owner;
    }

    public Owner toOwner(UpdateUserForm updateUserForm) {

        Owner owner = new Owner();
        owner.setVat(updateUserForm.getVat());
        owner.setAddress(updateUserForm.getAddress());
        owner.setEmail(updateUserForm.getEmail());
        owner.setOwnerType(OwnerType.valueOf(updateUserForm.getOwnerType()));
        owner.setPropertyType(PropertyType.valueOf(updateUserForm.getPropertyType()));
        owner.setPhoneNumber(updateUserForm.getPhoneNumber());
        owner.setFirstName(updateUserForm.getFirstName());
        owner.setLastName(updateUserForm.getLastName());
        owner.setPassword(passwordEncoder.encode(updateUserForm.getPassword()));

        return owner;
    }
}
