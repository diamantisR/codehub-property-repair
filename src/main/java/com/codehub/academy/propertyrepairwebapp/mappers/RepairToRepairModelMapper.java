package com.codehub.academy.propertyrepairwebapp.mappers;

import com.codehub.academy.propertyrepairwebapp.domain.Repair;
import com.codehub.academy.propertyrepairwebapp.model.RepairModel;
import org.springframework.stereotype.Component;

@Component
public class RepairToRepairModelMapper {

    public RepairModel mapToRepairModel(Repair repair) {

        RepairModel repairModel = new RepairModel();
        repairModel.setCost(repair.getCost());
        repairModel.setDescription(repair.getDescription());
        repairModel.setOwner(repair.getOwner().getFirstName() + " " + repair.getOwner().getLastName());
        repairModel.setOwnerLastName(repair.getOwner().getLastName());
        repairModel.setOwnerFirstName(repair.getOwner().getFirstName());
        repairModel.setRepairDate(repair.getRepairDate());
        repairModel.setRepairStatus(repair.getRepairStatus().getStatus());
        repairModel.setRepairType(repair.getRepairType().getType());
        repairModel.setOwnerVat(repair.getOwner().getVat());

        return repairModel;
    }
}


