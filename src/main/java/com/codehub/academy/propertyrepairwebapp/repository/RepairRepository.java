package com.codehub.academy.propertyrepairwebapp.repository;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.domain.Repair;
import com.codehub.academy.propertyrepairwebapp.enums.RepairStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface RepairRepository extends JpaRepository<Repair, Long> {
    List<Repair> findAll();
    Page<Repair> findAll(Pageable pageable);
    Optional<Repair> findById(Long id);
    Repair save(Repair repair);
    void deleteById(Long id);
    List<Repair> findByOwnerVat(String vat);

    List<Repair> findByOwnerId(Long id);
    List<Repair> findByRepairDateBetween(LocalDate startDate, LocalDate endDate);
    List<Repair> findByRepairDateLessThanEqual(LocalDate startDate);
    List<Repair> findByRepairDateGreaterThanEqual(LocalDate startDate);

    List<Repair> findByRepairStatusOrderByRepairDate(RepairStatus status);
}
