package com.codehub.academy.propertyrepairwebapp.model;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.enums.RepairStatus;
import com.codehub.academy.propertyrepairwebapp.enums.RepairType;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class RepairModel {
    private Long id;
    private String repairStatus;
    private String repairType;
    private int cost;
    private String description;
    private String owner;
    private String ownerFirstName;
    private String ownerLastName;
    private String ownerVat;
    private String ownerId;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate repairDate;

    public RepairModel(Long id, String repairStatus, String repairType, int cost, String description, String owner, String ownerFirstName,
                       String ownerLastName, String ownerVat, String ownerId, LocalDate repairDate) {
        this.id = id;
        this.repairStatus = repairStatus;
        this.repairType = repairType;
        this.cost = cost;
        this.description = description;
        this.owner = owner;
        this.ownerFirstName = ownerFirstName;
        this.ownerLastName = ownerLastName;
        this.ownerVat = ownerVat;
        this.ownerId = ownerId;
        this.repairDate = repairDate;
    }

    public RepairModel() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(String repairStatus) {
        this.repairStatus = repairStatus;
    }

    public String getRepairType() {
        return repairType;
    }

    public void setRepairType(String repairType) {
        this.repairType = repairType;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    public void setOwnerFirstName(String ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }

    public String getOwnerLastName() {
        return ownerLastName;
    }

    public void setOwnerLastName(String ownerLastName) {
        this.ownerLastName = ownerLastName;
    }

    public String getOwnerVat() {
        return ownerVat;
    }

    public void setOwnerVat(String ownerVat) {
        this.ownerVat = ownerVat;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public LocalDate getRepairDate() {
        return repairDate;
    }

    public void setRepairDate(LocalDate repairDate) {
        this.repairDate = repairDate;
    }

}
