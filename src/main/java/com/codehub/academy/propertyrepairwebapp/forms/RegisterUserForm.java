package com.codehub.academy.propertyrepairwebapp.forms;

import com.codehub.academy.propertyrepairwebapp.enums.OwnerType;
import com.codehub.academy.propertyrepairwebapp.enums.PropertyType;
import com.codehub.academy.propertyrepairwebapp.utils.GlobalAttributes;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.codehub.academy.propertyrepairwebapp.utils.GlobalAttributes.PASSWORD_PATTERN;

public class RegisterUserForm {

    @Pattern(regexp = GlobalAttributes.VAT_PATTERN, message = GlobalAttributes.VAT_PATTERN_MESSAGE)
    @Size(min = GlobalAttributes.VAT_LENGTH, max = GlobalAttributes.VAT_LENGTH, message = GlobalAttributes.VAT_LENGTH_MESSAGE)
    private String vat;

    @Pattern(regexp = GlobalAttributes.NAME_PATTERN, message = GlobalAttributes.NAME_PATTERN_MESSAGE)
    @Size(min = GlobalAttributes.NAME_SIZE, message = GlobalAttributes.NAME_PATTERN_MESSAGE)
    private String firstName;

    @Pattern(regexp = GlobalAttributes.NAME_PATTERN, message = GlobalAttributes.NAME_PATTERN_MESSAGE)
    @Size(min = GlobalAttributes.NAME_SIZE, message = GlobalAttributes.NAME_PATTERN_MESSAGE)
    private String lastName;

    @Size(min = GlobalAttributes.ADDRESS_SIZE, message = GlobalAttributes.ADDRESS_SIZE_MESSAGE)
    private String address;

    @Pattern(regexp = GlobalAttributes.PHONE_PATTERN, message = GlobalAttributes.PHONE_PATTERN_MESSAGE)
    private String phoneNumber;

    @Pattern(regexp = GlobalAttributes.EMAIL_PATTERN, message = GlobalAttributes.EMAIL_PATTERN_MESSAGE)
    private String email;

    @Pattern(regexp = PASSWORD_PATTERN, message = GlobalAttributes.PASSWORD_PATTERN_MESSAGE)
    @Size(min = GlobalAttributes.PASSWORD_SIZE, message = GlobalAttributes.PASSWORD_SIZE_MESSAGE)
    private String password;

    private String ownerType;

    private String propertyType;

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    @Override
    public String toString() {
        return "RegisterUserForm{" +
                "vat='" + vat + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", ownerType='" + ownerType + '\'' +
                ", propertyType='" + propertyType + '\'' +
                '}';
    }
}
