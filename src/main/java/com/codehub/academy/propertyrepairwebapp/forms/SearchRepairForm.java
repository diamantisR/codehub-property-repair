package com.codehub.academy.propertyrepairwebapp.forms;

public class SearchRepairForm {
    private String startDate;

    private String endDate;

    private String vat;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    @Override
    public String toString() {
        return "SearchForm{" +
                "startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", vat='" + vat + '\'' +
                '}';
    }
}
