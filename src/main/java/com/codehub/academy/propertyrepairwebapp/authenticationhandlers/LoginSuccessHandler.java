package com.codehub.academy.propertyrepairwebapp.authenticationhandlers;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.repository.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;


@Component
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    OwnerRepository ownerRepository;

    private static final String USER_HOME_PAGE_URL = "/user/home";
    private static final String ADMIN_HOME_PAGE_URL = "/admin/home";

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        HttpSession session = request.getSession();

        Owner owner = ownerRepository.findByEmail(authentication.getName());

        session.setAttribute("email", owner.getEmail());
        session.setAttribute("lastName", owner.getLastName());

        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        String redirectUrl = "/";
        for (GrantedAuthority grantedAuthority : authorities) {
            if (grantedAuthority.getAuthority().equals("ADMIN")) {
                redirectUrl = ADMIN_HOME_PAGE_URL;
            }
            if (grantedAuthority.getAuthority().equals("USER")) {
                redirectUrl = USER_HOME_PAGE_URL;
            }
        }
        redirectStrategy.sendRedirect(request, response, redirectUrl);
    }

}