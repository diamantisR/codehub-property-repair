package com.codehub.academy.propertyrepairwebapp.enums;

public enum PropertyType {

    DETOUCHED_HOUSE("Detouched House"),
    MAISONETTE("Maisonette"),
    APARTMENT_BUILDING("Apartment Building");

    private String type;

    PropertyType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
