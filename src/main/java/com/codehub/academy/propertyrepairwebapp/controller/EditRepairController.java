package com.codehub.academy.propertyrepairwebapp.controller;

import com.codehub.academy.propertyrepairwebapp.domain.Repair;
import com.codehub.academy.propertyrepairwebapp.forms.RepairForm;
import com.codehub.academy.propertyrepairwebapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class EditRepairController {
    private static final String REPAIR_FORM = "editRepairForm";
    @Autowired
    RepairService repairService;

    @GetMapping(value = "/admin/edit/{id}/repair")
    public String editRepair(@PathVariable Long id, Model model) {
        Repair repair = repairService.findById(id);
        model.addAttribute("repair", repair);
        model.addAttribute(REPAIR_FORM, new RepairForm());
        return "edit-repair";
    }

    @PostMapping(value = "/admin/repair/{id}/edit")
    public String editRepair(@PathVariable Long id, @Valid @ModelAttribute(REPAIR_FORM)
            RepairForm editRepairForm, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            Repair repair = repairService.findById(id);
            model.addAttribute("repair", repair);
            return "edit-repair";
        }

        repairService.updateRepair(editRepairForm, id);

        List<Repair> repairs = repairService.findAll();
        model.addAttribute("repairs", repairs);
        return "redirect:/admin/repairs";
    }


}
