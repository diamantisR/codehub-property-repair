package com.codehub.academy.propertyrepairwebapp.controller;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.exception.OwnerEmailExistsException;
import com.codehub.academy.propertyrepairwebapp.exception.OwnerVatExistsException;
import com.codehub.academy.propertyrepairwebapp.forms.RegisterUserForm;
import com.codehub.academy.propertyrepairwebapp.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegisterUserController {

    private static final String OWNER_FORM = "registerUserForm";

    @Autowired
    private OwnerService ownerService;

    @GetMapping(value = "/admin/create-owner")
    public String registerOwner(ModelMap model) {
        model.addAttribute(OWNER_FORM, new RegisterUserForm());
        return "create-owner";
    }

    @PostMapping({"/admin/create-owner"})
    public String registerOwner(@Valid @ModelAttribute(OWNER_FORM)
                                        RegisterUserForm registerUserForm, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "create-owner";
        }

        try {
            ownerService.createOwner(registerUserForm);
        } catch (OwnerEmailExistsException ex) {
            model.addAttribute("emailExists", ex.getMessage());
            return "create-owner";
        } catch (OwnerVatExistsException ex) {
            model.addAttribute("vatExists", ex.getMessage());
            return "create-owner";
        }

        return "redirect:/admin/owners";
    }
}
