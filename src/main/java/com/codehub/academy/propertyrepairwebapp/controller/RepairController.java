package com.codehub.academy.propertyrepairwebapp.controller;

import com.codehub.academy.propertyrepairwebapp.domain.Repair;
import com.codehub.academy.propertyrepairwebapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class RepairController {
    @Autowired
    RepairService repairService;

    @GetMapping({"/admin/repairs"})
    public String displayRepairs(Model model) {
        List<Repair> repairs = repairService.findAll();

        model.addAttribute("repairs", repairs);
        return "show-repairs";
    }
}
