package com.codehub.academy.propertyrepairwebapp.controller;

import com.codehub.academy.propertyrepairwebapp.domain.Repair;
import com.codehub.academy.propertyrepairwebapp.service.OwnerService;
import com.codehub.academy.propertyrepairwebapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {
    @Autowired
    RepairService repairService;

    @Autowired
    OwnerService ownerService;


    @GetMapping("/")
    public String home() {
        if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals("ADMIN"))) {
            return "redirect:/admin/home";
        }
        else if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals("USER"))) {
            return "redirect:/user/home";
        }
        return "index";
    }


    @GetMapping("/admin/home")
    public String adminHome(Model model) {
        List<Repair> ongoingRepairs = repairService.findOngoingRepairs();
        model.addAttribute("repairs", ongoingRepairs);
        return "admin-home";
    }

    @GetMapping("/user/home")
    public String userHome(Model model) {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        Long id = ownerService.findByEmail(email).getId();

        List<Repair> ownerRepairs = repairService.findOwnerRepairs(id);
        model.addAttribute("repairs", ownerRepairs);
        return "user-home";
    }

}
