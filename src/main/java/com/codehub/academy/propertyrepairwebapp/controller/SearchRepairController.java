package com.codehub.academy.propertyrepairwebapp.controller;

import com.codehub.academy.propertyrepairwebapp.domain.Repair;
import com.codehub.academy.propertyrepairwebapp.forms.SearchRepairForm;
import com.codehub.academy.propertyrepairwebapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class SearchRepairController {

    @Autowired
    private RepairService repairService;

    @GetMapping("/admin/search-repair")
    public String searchTest(ModelMap modelMap) {
        modelMap.addAttribute("searchRepairForm", new SearchRepairForm());
        return "search-repair";
    }

    @PostMapping("/admin/search-repair")
    public String searchTest(@ModelAttribute("searchRepairForm")
                                     SearchRepairForm searchRepairForm, ModelMap modelMap) {

        List<Repair> repairs = repairService.searchRepair(searchRepairForm);

        if (!repairs.isEmpty()) {
            modelMap.addAttribute("repairs", repairs);
        } else {
            modelMap.addAttribute("repairs", null);
        }

        return "show-repairs";
    }
}
