package com.codehub.academy.propertyrepairwebapp.service;

import com.codehub.academy.propertyrepairwebapp.domain.Repair;
import com.codehub.academy.propertyrepairwebapp.forms.RepairForm;
import com.codehub.academy.propertyrepairwebapp.forms.SearchRepairForm;
import com.codehub.academy.propertyrepairwebapp.model.RepairModel;

import java.util.List;
import java.util.Optional;

public interface RepairService {

    Repair findById(Long id);

    Repair createRepair(RepairForm repairForm);

    Repair updateRepair(RepairForm repairForm, Long id);

    void deleteById(Long id);

    Optional<RepairModel> findRepair(Long id);

    List<Repair> findAll();

    List<Repair> findRepairsByVat(String vat);

    List<String> getOwnerVats();

    List<Repair> searchRepair(SearchRepairForm searchForm);

    List<Repair> findOngoingRepairs();

    List<Repair> findOwnerRepairs(Long id);
}