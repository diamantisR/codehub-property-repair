package com.codehub.academy.propertyrepairwebapp;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
class PropertyRepairWebappApplicationTests {
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertyRepairWebappApplicationTests.class);
	List<String> passwordsToBeHased = Arrays.asList("12345","61641","45554","55555");

	@Autowired
	private SecurityConfig securityConfig;

	@Test
	void contextLoads() {
		passwordsToBeHased.forEach(password ->
				LOGGER.info("Hash value of password " + password + " is :" + securityConfig.passwordEncoder().encode(password)));
	}

}
